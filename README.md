# SA8155P Automotive Development Platform Gadget Snap

This repository contains the Ubuntu Core gadget snap for the SA8155P Automotive Development Platform

## Gadget Snaps

Gadget snaps are a special type of snaps that contain device specific support
code and data. You can read more about them in the snapd wiki
https://github.com/snapcore/snapd/wiki/Gadget-snap

## Building
To build the gadget snap locally please use `snapcraft`.

Build has to be supplied with additional build dependencies. Those are provided
as env variables and can be either absolute path to local directory,
or git repository. Git repo can include `-b <branch name>` and other git parameters.
Compulsory evn variables:
 - SA8155P_GADGET_SNAP_BOOT_ASSETS: prebuilt boot assets

Optional env variables:
 - SA8155P_GADGET_SNAP_SECTOOLS: sectools with configured signing key
 - SA8155P_GADGET_SNAP_EDK2: override edk2 to use
 - SA8155P_GADGET_SNAP_HOOKS: custom hooks to be included
 - SA8155P_GADGET_SNAP_ENABLE_SEC_ELF: set to 1 to build sec.elf (fuse blower)

## Boot assets signing
If sectools are not provided, included boot assets are signed with key configured
in passed sectools.
If no sectools are passed, no boot assets are signed and created snap version
has appended '-unsigned'.

## Signed boot assets in existing snap
Boot assets in 'unsigned' snap can be signed later using included sign-boot-assets
helper script.
Run as:
$ ./sign-boot-assets sign-repack \
   --sectools <path to the sectools with configured signing key>
   --config <path to the sectools config to be used for the signing>
   --gadget-snap <path to the existing gadget snap to be signed and repacked>
   [--workdir <optional working directory>]

## Secure boot
If `SA8155P_GADGET_SNAP_ENABLE_SEC_ELF=1` is declared during building, the `sec.elf`
(fuse blower) will be built as well.
When running the `flashall` script, it will flash the `sec.elf` to the `secdata`
partition to blow fuses and enable secure boot.

If `SA8155P_GADGET_SNAP_ENABLE_SEC_ELF` is not defined or is not equal to 1, the
recipe still builds the fuse blower, but the filename will be `sec.elf.disabled`.
Users can rename it manually to enable secure boot.
