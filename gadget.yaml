# SPDX-License-Identifier: GPL-2.0
# Copyright (C) 2021 Canonical Ltd
device-tree-origin: kernel
volumes:
  lun-0:
    schema: gpt
    bootloader: lk
    structure:
      - name: ssd
        size: 8192
        type: 2C86E742-745E-4FDD-BFD8-B6A7AC638772
      - name: persist
        size: 10M
        type: 6C95E238-E343-4BA8-B489-8681ED22AD0B
      - name: misc
        size: 1M
        type: 04600328-0CC3-47B8-8EF1-B192C39E3E1F
      - name: ubuntu-seed
        filesystem: ext4
        size: 500M
        type: 0FC63DAF-8483-4772-8E79-3D69D8477DE4
        role: system-seed
      - name: ubuntu-save
        filesystem: ext4
        size: 100M
        type: 0FC63DAF-8483-4772-8E79-3D69D8477DE4
        role: system-save
      - name: ubuntu-boot
        filesystem: ext4
        size: 10M
        type: 0FC63DAF-8483-4772-8E79-3D69D8477DE4
        role: system-boot
      - name: ubuntu-data
        filesystem: ext4
        size: 1G
        type: 0FC63DAF-8483-4772-8E79-3D69D8477DE4
        role: system-data
  lun-1:
    schema: gpt
    structure:
      - name: xbl_a
        size: 3670016
        type: DEA0BA2C-CBDD-4805-B4F9-F428251C3E98
        content:
            - image: blobs/xbl.elf
      - name: xbl_config_a
        size: 131072
        type: 5A325AE4-4276-B66D-0ADD-3494DF27706A
        content:
            - image: blobs/xbl_config.elf
  lun-2:
    schema: gpt
    structure:
      - name: xbl_b
        size: 3670016
        type: DEA0BA2C-CBDD-4805-B4F9-F428251C3E98
        content:
            - image: blobs/xbl.elf
      - name: xbl_config_b
        size: 131072
        type: 5A325AE4-4276-B66D-0ADD-3494DF27706A
        content:
            - image: blobs/xbl_config.elf
  lun-3:
    schema: gpt
    structure:
      - name: ALIGN_TO_128K_1
        size: 106496
        type: FDE1604B-D68B-4BD4-973D-962AE7A1ED88
      - name: cdt
        size: 131072
        type: A19F205F-CCD8-4B6D-8F1E-2D9BC24CFFB1
        content:
            - image: blobs/adp_1_1.0.bin
      - name: ddr
        size: 1M
        type: 20A0C19C-286A-42FA-9CE7-F64C3226A794
  lun-4:
    schema: gpt
    structure:
      # These are the 'A' partition's needed for the A/B boot/ota update feature
      # If you add something to this section remember to add it to B as well
      - name: aop_a
        size: 524288
        type: D69E90A5-4CAB-0071-F6DF-AB977F141A7F
        content:
            - image: blobs/aop.mbn
      - name: tz_a
        size: 4M
        type: A053AA7F-40B8-4B1C-BA08-2F68AC71A4F4
        content:
            - image: blobs/tz.mbn
      - name: hyp_a
        size: 524288
        type: E1A6A689-0C8D-4CC6-B4E8-55A4320FBD8A
        content:
            - image: blobs/hyp.mbn
      - name: mdtpsecapp_a
        size: 4M
        type: EA02D680-8712-4552-A3BE-E6087829C1E6
      - name: mdtp_a
        size: 32M
        type: 3878408A-E263-4B67-B878-6340B35B11E3
      - name: abl_a
        size: 1M
        type: BD6928A1-4CE0-A038-4F3A-1495E3EDDFFB
        content:
            - image: blobs/abl.elf
      - name: keymaster_a
        size: 524288
        type: A11D2A7C-D82A-4C2F-8A01-1805240E6626
        content:
            - image: blobs/km4.mbn
      - name: boot-ra
        size: 100M
        type: 20117F86-E985-4357-B9EE-374BC1D8487D
        role: system-seed-image
      - name: boot-a
        size: 100M
        type: 20117F86-E985-4357-B9EE-374BC1D8487D
        role: system-boot-image
      - name: cmnlib_a
        size: 524288
        type: 73471795-AB54-43F9-A847-4F72EA5CBEF5
        content:
            - image: blobs/cmnlib.mbn
      - name: cmnlib64_a
        size: 524288
        type: 8EA64893-1267-4A1B-947C-7C362ACAAD2C
        content:
            - image: blobs/cmnlib64.mbn
      - name: devcfg_a
        size: 131072
        type: F65D4B16-343D-4E25-AAFC-BE99B6556A6D
        content:
            - image: blobs/devcfg_auto.mbn
      - name: qupfw_a
        size: 77824
        type: 21d1219f-2ed1-4ab4-930a-41a16ae75f7f
        content:
            - image: blobs/qupv3fw.elf
      - name: vendor_a
        size: 100M
        type: 97D7B011-54DA-4835-B3C4-917AD6E73D74
      - name: dtbo-ra
        size: 24M
        type: 24d0d418-d31d-4d8d-ac2c-4d4305188450
      - name: dtbo-a
        size: 24M
        type: 24d0d418-d31d-4d8d-ac2c-4d4305188450
      - name: uefisecapp_a
        size: 2M
        type: BE8A7E08-1B7A-4CAE-993A-D5B7FB55B3C2
        content:
            - image: blobs/uefi_sec.mbn
      - name: snaprecoverysel
        size: 131072
        type: B214D5E4-D442-45E6-B8C6-01BDCD82D396
        role: system-seed-select
        content:
            - image: snaprecoverysel.bin
      - name: snapbootsel
        size: 131072
        type: B214D5E4-D442-45E6-B8C6-01BDCD82D396
        role: system-boot-select
        content:
            - image: blobs/snapbootsel.bin
      # These are the 'B' partition's needed for the A/B boot/ota update feature.
      # A and B partitions must have differrent GUID's.
      # For convinience sake we keep all the B partitions with the same GUID"
      - name: aop_b
        size: 524288
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: tz_b
        size: 4M
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: hyp_b
        size: 524288
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: mdtpsecapp_b
        size: 4M
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: mdtp_b
        size: 32M
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: abl_b
        size: 1M
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: keymaster_b
        size: 524288
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: boot-rb
        size: 100M
        type: 20117F86-E985-4357-B9EE-374BC1D8487D
        role: system-seed-image
      - name: boot-b
        size: 100M
        type: 20117F86-E985-4357-B9EE-374BC1D8487D
        role: system-boot-image
      - name: cmnlib_b
        size: 524288
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: cmnlib64_b
        size: 524288
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: devcfg_b
        size: 131072
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: qupfw_b
        size: 77824
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: vendor_b
        size: 100M
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: dtbo-rb
        size: 24M
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: dtbo-b
        size: 24M
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: uefisecapp_b
        size: 2M
        type: 77036CD4-03D5-42BB-8ED1-37E5A88BAA34
      - name: snaprecoveryselbak
        size: 131072
        type: B214D5E4-D442-45E6-B8C6-01BDCD82D396
        role: system-seed-select
        content:
            - image: snaprecoverysel.bin
      - name: snapbootselbak
        size: 131072
        type: B214D5E4-D442-45E6-B8C6-01BDCD82D396
        role: system-boot-select
        content:
            - image: blobs/snapbootsel.bin
      # These are non A/B partitions. In a A/B build these would not be updated via a OTA update
      - name: devinfo
        size: 4096
        type: 65ADDCF4-0C5C-4D9A-AC2D-D90B5CBFCD03
      - name: dip
        size: 1M
        type: 4114B077-005D-4E12-AC8C-B493BDA684FB
      - name: apdp
        size: 262144
        type: E6E98DA2-E22A-4D12-AB33-169E7DEAA507
      - name: spunvm
        size: 8M
        type: e42e2b4c-33b0-429b-b1ef-d341c547022c
      - name: splash
        size: 34226176
        type: AD99F201-DC71-4E30-9630-E19EEF553D1B
      - name: limits
        size: 4096
        type: 10A0C19C-516A-5444-5CE3-664C3226A794
      - name: toolsfv
        size: 1M
        type: 97745ABA-135A-44C3-9ADC-05616173C24C
      - name: logfs
        size: 8M
        type: BC0330EB-3410-4951-A617-03898DBE3372
        content:
            - image: blobs/logfs_ufs_8mb.bin
      - name: cateloader
        size: 2M
        type: AA9A5C4C-4F1F-7D3A-014A-22BD33BF7191
      - name: rawdump
        size: 128M
        type: 66C9B323-F7FC-48B6-BF96-6F32E335A428
      - name: emac
        size: 524288
        type: e7e5eff9-d224-4eb3-8f0b-1d2a4be18665
      - name: logdump
        size: 64M
        type: 5AF80809-AABB-4943-9168-CDFC38742598
      - name: storsec
        size: 131072
        type: 02DB45FE-AD1B-4CB6-AECC-0042C637DEFA
        content:
            - image: blobs/storsec.mbn
      - name: multiimgoem
        size: 32768
        type: E126A436-757E-42D0-8D19-0F362F7A62B8
        content:
            - image: blobs/multi_image.mbn
      - name: multiimgqti
        size: 32768
        type: 846C6F05-EB46-4C0A-A1A3-3648EF3F9D0E
      - name: uefivarstore
        size: 524288
        type: 165BD6BC-9250-4AC8-95A7-A93F4A440066
      - name: secdata
        size: 28672
        type: 76cfc7ef-039d-4e2c-b81e-4dd8c2cb2a93
      - name: catefv
        size: 524288
        type: 80c23c26-c3f9-4a19-bb38-1e457daceb09
      - name: catecontentfv
        size: 1M
        type: e12d830b-7f62-4f0b-b48a-8178c5bf3ac1

  lun-5:
    schema: gpt
    structure:
      - name: ALIGN_TO_128K_2
        size: 106496
        type: 6891A3B7-0CCC-4705-BB53-2673CAC193BD
      - name: modemst1
        size: 2M
        type: EBBEADAF-22C9-E33B-8F5D-0E81686A68CB
      - name: modemst2
        size: 2M
        type: 0A288B1F-22C9-E33B-8F5D-0E81686A68CB
      - name: fsg
        size: 2M
        type: 638FF8E2-22C9-E33B-8F5D-0E81686A68CB
      - name: fsc
        size: 131072
        type: 57B90A16-22C9-E33B-8F5D-0E81686A68CB

defaults:
  # system default settings
  system:
    refresh:
      retain: 2
